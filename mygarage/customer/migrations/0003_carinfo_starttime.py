# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_remove_carinfo_starttime'),
    ]

    operations = [
        migrations.AddField(
            model_name='carinfo',
            name='starttime',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
    ]
