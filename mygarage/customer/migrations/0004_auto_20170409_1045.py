# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0003_carinfo_starttime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carinfo',
            name='starttime',
            field=models.DateTimeField(verbose_name=b'date checkin'),
        ),
    ]
