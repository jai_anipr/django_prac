from django.shortcuts import render,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader
from .models import carinfo
from django.contrib import auth



from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required


def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c)


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/home/customer/loggedin')
    else:
        return HttpResponseRedirect('/home/customer/invalid')


def loggedin(request):
    return render_to_response('loggedin.html',
                              {'full_name': request.user.username})


def invalid_login(request):
    return render_to_response('invalid_login.html')


def logout(request):
    auth.logout(request)
    return render_to_response('logout.html')













def cust_login(request):
    next = request.GET.get('next', '/home/')

    '''
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next)
            else:
                return HttpResponse("Inactive user.")
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)
    '''
    return render(request, "customer/index_customer.html", {'redirect_to': next})



""" all_cars=carinfo.objects.all()
    context= {
        'all_cars':all_cars,
    }
    return render(request,'customer/index_customer.html',context)
"""

def homepage(request):
    return render(request,'customer/index_homepage.html')

def staff_login(request):
    all_cars=carinfo.objects.all()
    context={
        'all_cars':all_cars,
    }
    return render(request,'customer/index.html',context)


def detail(request,c_id):
    all_cars=carinfo.objects.all()
    num_cars= carinfo.objects.all().count()
    template=loader.get_template('customer/index1.html')
    context={
        'all_cars':all_cars,
    }
    return HttpResponse(template.render(context))




    #return HttpResponse("<h1>The car has id of:"+str(c_id)+"</h1>")



