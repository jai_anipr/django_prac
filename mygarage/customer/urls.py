"""mygarage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth import views as auth_views

app_name = 'customer'

urlpatterns = [
    url(r'^customer$',views.cust_login,name='cust_login'),
    url(r'^staff$',views.staff_login,name='staff_login'),
    url(r'^(?P<c_id>[0-9]+)/$',views.detail,name='detail'),
    url(r'^$',views.homepage,name='homepage'),
    url(r'^customer/login/$', 'customer.views.login'),
    url(r'^/auth/$', 'customer.views.auth_view'),
    url(r'^customer/logout/$', 'customer.views.logout'),
    url(r'^customer/loggedin/$', 'customer.views.loggedin'),
    url(r'^customer/invalid/$', 'customer.views.invalid_login'),

]
